import { Component, OnInit } from '@angular/core';
import { PequeteService } from '../pequete.service';
import { FormBuilder} from '@angular/forms';


@Component({
  selector: 'app-paquete-create',
  templateUrl: './paquete-create.component.html',
  styleUrls: ['./paquete-create.component.sass']
})
export class PaqueteCreateComponent implements OnInit {

  paqueteWithClient: IPaqueteWithClient;

  constructor(private formBuilder: FormBuilder,private PaqueteService: PequeteService ) { }

  ngOnInit():void {
  }

}
