import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaqueteCreateComponent } from './paquete-create/paquete-create.component';
import { PaqueteUpdateComponent } from './paquete-update/paquete-update.component';
import { PaqueteListComponent } from './paquete-list/paquete-list.component';


const routes: Routes = [
  {
    path: 'paquete-create',
    component:PaqueteCreateComponent
  },
  {
    path: 'paquete-list',
    component:PaqueteListComponent
  },
  {
    path: 'paquete-update',
    component:PaqueteUpdateComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
