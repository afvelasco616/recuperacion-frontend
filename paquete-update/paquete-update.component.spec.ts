import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaqueteUpdateComponent } from './paquete-update.component';

describe('PaqueteUpdateComponent', () => {
  let component: PaqueteUpdateComponent;
  let fixture: ComponentFixture<PaqueteUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaqueteUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaqueteUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
