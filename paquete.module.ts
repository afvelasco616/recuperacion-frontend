import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaqueteListComponent } from './paquete-list/paquete-list.component';
import { PaqueteUpdateComponent } from './paquete-update/paquete-update.component';
import { PaqueteCreateComponent } from './paquete-create/paquete-create.component';



@NgModule({
  declarations: [
    PaqueteListComponent,
    PaqueteUpdateComponent,
    PaqueteCreateComponent],
  imports: [
    CommonModule
  ]
})
export class PaqueteModule { }
