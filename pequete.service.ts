import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { IPaquete } from './paquete.interfas';
import { create } from 'domain';
import { createRequestOption } from 'src/app/utils/request_utils';
import { environment } from 'src/environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PequeteService {

  constructor(private http: HttpClient) {}

    public query(req?: any): Observable<IPaquete[]> {
      const params = createRequestOption(req);
      return this.http.get<IPaquete[]>(`${environment.END_POINT}/api/paquete`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }

    public getPaqueteById(id: string): Observable<IPaquete> {
      return this.http.get<IPaquete>(`${environment.END_POINT}/api/paquete/${id}`)
      .pipe(map(res => {
        return res;
      }));
    }

    public updatePaquete(paquete: IPaquete): Observable<IPaquete> {
      return this.http.put<IPaquete>(`${environment.END_POINT}/api/paquete`, paquete)
      .pipe(map(res => {
        return res;
      }));
    }

}